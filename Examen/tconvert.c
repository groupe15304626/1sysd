#include <stdio.h>
#include <stdlib.h>

int celsius2fahrenheit()
{
    float celsius, fahrenheit;

    printf("\nEntrer la temperature en Celsius : ");
    scanf("%f", &celsius);

    fahrenheit = (1.8 * celsius) + 32;

    printf("\nTemperature en Fahrenheit : %f ", fahrenheit);

    return 0;
}

int fahrenheit2celsius()
{
    float celsius, fahrenheit;

    printf("\nEntrer la temperature en Fahrenheit : ");
    scanf("%f", &fahrenheit);

    celsius = (fahrenheit - 32) * 5 / 9;

    printf("\nTemperature en Celsius : %f ", celsius);

    return 0;
}

int main()
{
    int choix;
    printf("\nPour C° vers F° taper 1 pour l'inverse taper 2 : ");
    scanf("%d", &choix);

    if (choix == 1)
    {
        celsius2fahrenheit();
    }
    else if (choix == 2)
    {
        fahrenheit2celsius();
    }
    else
    {
        printf("\nCeci est une erreur");
    }

    return 0;
}
