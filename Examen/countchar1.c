#include <stdio.h>

int count_char(const char *str, char ch)
{
    int count = 0;
    while (*str != '\0')
    {
        if (*str == ch)
        {
            count++;
        }
        str++;
    }
    return count;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("Usage: %s <caractère> <chaîne>\n", argv[0]);
        return 1;
    }

    char character_to_count = argv[1][0];
    const char *input_string = argv[2];

    int result = count_char(input_string, character_to_count);
    printf("%d\n", result);

    return 0;
}
