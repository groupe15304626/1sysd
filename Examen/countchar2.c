#include <stdio.h>
#include <string.h>
#include <ctype.h>

int count_char(const char *str, char ch, int ignore_case)
{
    int count = 0;
    while (*str != '\0')
    {
        if ((ignore_case && tolower(*str) == tolower(ch)) || (!ignore_case && *str == ch))
        {
            count++;
        }
        str++;
    }
    return count;
}

int main(int argc, char *argv[])
{
    if (argc < 3 || argc > 4)
    {
        printf("Usage: %s <caractère> <chaîne> [-i]\n", argv[0]);
        return 1;
    }

    char character_to_count = argv[1][0];
    const char *input_string = argv[2];
    int ignore_case = 0;

    if (argc == 4)
    {
        if (strcmp(argv[3], "-i") == 0)
        {
            ignore_case = 1;
        }
        else
        {
            printf("Usage: %s <caractère> <chaîne> [-i]\n", argv[0]);
            return 1;
        }
    }

    int result = count_char(input_string, character_to_count, ignore_case);
    printf("%d\n", result);

    return 0;
}
