#include <stdio.h>
#include <stdlib.h>

typedef struct Node Node;
struct Node {
    int value;
    Node *next;
};

void print_list(Node *head) {
    Node *current;

    current = head;
    while (current) {
        printf("%d ", current->value);
        current = current->next;
    }
    printf("\n");
}

Node *new_node(int value) {
    Node *node;
    node = malloc(sizeof(Node));
    node->value = value;
    node->next = NULL;
    return node;
}

Node *append_value(Node *head, int value) {
    Node *current;

    current = head;

    // deux cas  possible : la liste est vide (head est NULL)
    // ou pas...
    if (head == NULL) {
        head = new_node(value);
    } else { // la liste n'est pas vide
        current = head;
        while (current->next) {
            current = current->next;
        }
        // arrivé ici on est sur un noeud qui n'a pas de successeur
        current->next = new_node(value);
    }
    return head;
}

int length(Node *head) {
    Node *current;
    int len = 0;

    current = head;
    while (current) {
        len++;
        current = current->next;
    }
    return len;
}

int total(Node *head) {
    Node *current;
    int sum = 0;

    current = head;
    while (current) {
        sum += current->value;
        current = current->next;
    }

    return sum;
}

float average(Node *head) {
    Node *current;
    int sum = 0;
    int count = 0;

    current = head;
    while (current) {
        sum += current->value;
        count++;
        current = current->next;
    }

    if (count == 0) {
        fprintf(stderr, "Erreur: la liste est vide.\n");
        exit(1);
    }

    return (float)sum / count;
}

Node *insert_value(Node *head, int value) {
    Node *new_head = new_node(value);
    new_head->next = head;
    return new_head;
}

int main() {
    int N;
    Node *head = NULL;

    // head est mis à jour seulement lors du premier appel
    head = append_value(head, 42);
    head = append_value(head, 4);
    head = append_value(head, 5);
    head = append_value(head, 42);
    head = append_value(head, 1);
    head = insert_value(head, 6);

    print_list(head);
    printf("Longueur de la liste : %d.\n", length(head));
    printf("Somme des valeurs de la liste : %d.\n", total(head));
    printf("Moyenne des valeurs de la liste : %.2f.\n", average(head));

    exit(0);
}
