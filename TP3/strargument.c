#include <stdio.h>

int strlen(const char *str) {
    int length = 0;

    // Parcours la chaîne jusqu'à atteindre le caractère nul '\0'
    while (*str != '\0') {
        length++;
        str++; // Déplace le pointeur vers le caractère suivant
    }

    return length;
}

int main() {	
    const char *myString = "Hello, world!";
    int length = strlen(myString);
    printf("Longueur de la chaîne : %d\n", length);

    return 0;
}
